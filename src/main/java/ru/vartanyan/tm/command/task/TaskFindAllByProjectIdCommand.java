package ru.vartanyan.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vartanyan.tm.command.AbstractTaskCommand;
import ru.vartanyan.tm.exception.system.NotLoggedInException;
import ru.vartanyan.tm.model.Task;
import ru.vartanyan.tm.util.TerminalUtil;

import java.util.List;

public class TaskFindAllByProjectIdCommand extends AbstractTaskCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public @NotNull String name() {
        return "task-find-all-by-project-id";
    }

    @Override
    public String description() {
        return "Find all tasks by project Id";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[FIND ALL TASKS BY PROJECT ID]");
        if (serviceLocator.getAuthService().isNotAuth()) throw new NotLoggedInException();
        @NotNull final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[ENTER PROJECT ID]");
        @NotNull final String projectId = TerminalUtil.nextLine();
        @Nullable final List<Task> tasks = serviceLocator.getProjectTaskService().findAllTaskByProjectId(projectId, userId);
        int index = 1;
        for (@Nullable final Task task: tasks){
            System.out.println(index + ". " + task);
            index++;
        }
        System.out.println("[OK]");
    }

}
